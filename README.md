# Desplegando Angular en Firebase

## Qué es Firebase
Firebase surgió como un sistema de base de datos en tiempo real que ha ido evolucionando añadiendo cada vez más servicios en la nube, sobre todo desde su compra por parte de Google en 2014.

Entre las funcionalidades que ofrece, se encuentran el sistema de analíticas, de monetización, hosting, autenticación, base de datos, … Todo ello en la nube y perfectamente integrados, de forma que se desde esta plataforma se pueda  gestionar todo lo relacionado con nuestra aplicación web o móvil.

Aunque se trata de un servicio de pago, esta herramienta  ofrece una versión gratuita con ciertos límites que podemos utilizar para proyectos pequeños o como punto de partida, de forma que si nuestra aplicación realmente tiene el éxito que esperamos, podremos de forma fácil ampliar nuestra cuenta a una de pago.

Como se trata de servicios en la nube, una de sus principales características es la escalabilidad, de forma que no tengamos que preocuparnos de aspectos técnicos dado que dicha escalabilidad se lleva a cabo de forma automática.

Además, las APIs, SDKs y documentación que ofrece Firebase hacen muy sencillo su uso e integración.

## Creando un proyecto en Firebase
Para comenzar con este tutorial, empezaremos accediendo a la consola de Firebase con nuestra cuenta de Google. Una vez dentro, crearemos un nuevo proyecto, indicando para ello simplemente un nombre:


![](./imagenes/Selección_169.png)


Al crear el proyecto, consultaremos las variables de configuración del mismo. Éstas nos harán falta más tarde para conectar nuestra aplicación el servicio:

![](./imagenes/Selección_170.png)


## Conectando Angular con Firebase
Ahora que ya tenemos creado nuestro proyecto, vamos a nuestra aplicación Angular. Creamos la aplicación, E instalamos la librería de Angular Firebase:

![](./imagenes/Selección_171.png)

Continuamos configurando los archivos de entornos con la configuración que antes obtuvimos en la consola de Firebase (se puede indicar una configuración para desarrollo y otra para producción en los distintos archivos de entornos, para lo que se necesite también crear dos proyectos; en nuestro caso utilizaremos la misma configuración para ambos entornos):

![](./imagenes/Selección_172.png)


Seguimos añadiendo la importación del módulo de AngularFire al módulo principal de la aplicación:

![](./imagenes/Selección_173.png)


Ahora que ya tenemos configurada nuestra aplicación, volvemos a la consola de Firebase y consultamos la sección de Hosting. Éste será el servicio que utilizaremos para almacenar nuestra aplicación.

En la pantalla que nos aparece, hacemos clic en “Comenzar” y nos mostrará unas instrucciones que seguiremos para desplegar la aplicación desde nuestro equipo.

![](./imagenes/Selección_174.png)

![](./imagenes/Selección_175.png)

Comenzamos instalando la librería firebase-tools:

![](./imagenes/Selección_176.png)

El siguiente paso es abrir un terminal en nuestro equipo y loguearnos en el servicio de Google:

![](./imagenes/Selección_177.png)


Una vez iniciada sesión, seguimos iniciando nuestro proyecto como proyecto de Firebase:

Al ejecutar este comando debemos responder varias preguntas:

La característica o servicio que queremos utilizar es “Hosting”.
Seleccionamos, de entre los proyectos que tenemos en Firebase, aquel que queremos asociar.
Como directorio público, debemos indicar “dist/NOMBRE_DE_TU_PROYECTO”. En mi caso, “dist/angular-firebase”.
A la pregunta de si queremos configurar nuestra aplicación como una Single Page Application (SPA), respondemos que sí.
Y ya tenemos conectado nuestro proyecto con Firebase. Estos pasos sólo deberemos llevarlos a cabo una vez.

![](./imagenes/Selección_178.png)


## Desplegando en Firebase
Para ver el resultado, pasamos a compilar el proyecto y desplegarlo:

![](./imagenes/Selección_179.png)



Por último, en la consola se os muestra el enlace al que podéis acceder para ver el resultado online del despliegue:

![](./imagenes/Selección_180.png)

![](./imagenes/Selección_181.png)


<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>